#!/usr/bin/python

from __future__ import print_function

import bmemcached
from datetime import datetime
import os
import praw
from requests import HTTPError
import sys
import textwrap


class WelcomeBot:
  version = '0.1'
  username = os.environ['REDDIT_USERNAME']
  password = os.environ['REDDIT_PASSWORD']
  ageThreshold = 30
  reply = textwrap.dedent("""
    Welcome to Reddit!
  """)

  def __init__(self, subReddits):
    self.reddit = praw.Reddit(
      '{username} {version}'.format(username=self.username, version=self.version)
    )
    self.reddit.login(self.username, self.password)

    self.commentCache = bmemcached.Client(
      (os.environ['MEMCACHEDCLOUD_SERVERS'],),
      os.environ['MEMCACHEDCLOUD_USERNAME'],
      os.environ['MEMCACHEDCLOUD_PASSWORD']
    )

    self.listen(subReddits)

  def listen(self, subReddits):
    for comment in praw.helpers.comment_stream(self.reddit, '+'.join(subReddits)):
      commentId = str(comment.id)

      if comment.author != self.username and not self.commentCache.get(commentId) and not self.commentCache.get('author-' + str(comment.author)):
        self.commentCache.set(commentId, True)

        try:
          redditor = self.reddit.get_redditor(comment.author)
        except HTTPError as error:
          print("HTTP Error:\n" + str(error), file=sys.stderr)
          continue
        except praw.errors.RateLimitExceeded as error:
           print("Rate Limit Exceeded:\n" + str(error), file=sys.stderr)
           time.sleep(err.sleep_time) 

        redditorAge = datetime.now() - datetime.utcfromtimestamp(redditor.created_utc)
        print(commentId + ' ' + str(redditorAge.days), file=sys.stderr)

        if redditorAge.days <= self.ageThreshold:
          self.commentCache.set('author-' + str(comment.author), True)

          try:
            comment.reply(self.reply)
          except HTTPError as error:
            print("HTTP Error:\n" + str(error), file=sys.stderr)
            continue
          except praw.errors.RateLimitExceeded as error:
             print("Rate Limit Exceeded:\n" + str(error), file=sys.stderr)
             time.sleep(error.sleep_time) 


WelcomeBot(['sandboxtest'])



